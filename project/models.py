from django.db import models
from datetime import datetime
from datetime import date
# User
from django.contrib.auth.models import User


def project_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/img/project/<filename>
    return 'img/project/{0}'.format(filename)


def profile_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/img/profile/<staff_id>/<filename>
    return 'img/profile/{0}/{1}'.format(instance.id, filename)


class Staff(models.Model):
    user = models.OneToOneField(User, related_name='staff', on_delete=models.CASCADE, blank=True, null=False)
    photo = models.ImageField(upload_to=profile_path, default='', blank=True, null=True)
    biography = models.TextField(max_length=500, blank=True, null=True)
    role = models.ForeignKey('Role', on_delete=models.SET_NULL, blank=True, null=True)
    birth_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.user.username


class Project(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(default='')
    status = models.CharField(max_length=255)
    priority = models.CharField(max_length=255)
    created = models.DateTimeField(default=datetime.now)
    git_link = models.CharField(max_length=255)
    language = models.CharField(max_length=255)
    created_by = models.ForeignKey('Staff', related_name='project_created_by', on_delete=models.SET_NULL, null=True)
    developers = models.ManyToManyField(Staff, through='ProjectStaff')

    def __str__(self):
        return self.name


class Issue(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(default='')
    status = models.CharField(max_length=255)
    priority = models.CharField(max_length=255)
    created = models.DateTimeField(default=datetime.now)
    last_update = models.DateTimeField(default=datetime.now)
    created_by = models.ForeignKey('Staff', related_name='issue_created_by', on_delete=models.SET_NULL, null=True)
    project = models.ForeignKey('Project', related_name="project_issue", on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.name


class Event(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(default='')
    location = models.CharField(max_length=255)
    date = models.DateField(blank=True, default=date.today())
    category = models.CharField(max_length=255)
    assistance = models.BooleanField(default=False)
    created_by = models.ForeignKey('Staff', related_name='event_created_by', on_delete=models.SET_NULL, null=True)
    participants = models.ManyToManyField('Staff', through='EventStaff', blank=True)

    def __str__(self):
        return self.name


class Message(models.Model):
    subject = models.CharField(max_length=255)
    text = models.TextField(default='')
    date = models.DateTimeField(blank=True, default=datetime.now())
    read = models.BooleanField(default=False)
    urgent = models.BooleanField(default=False)
    receiver = models.ForeignKey('Staff', related_name='receiver', on_delete=models.SET_NULL, null=True)
    sender = models.ForeignKey('Staff', related_name='sender', on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.subject


class Comment(models.Model):
    text = models.TextField(default='')
    date = models.DateTimeField(default=datetime.now)
    author = models.ForeignKey('Staff', related_name='comment_author', on_delete=models.SET_NULL, null=True)
    issue = models.ForeignKey('Issue', default="", related_name="issue_comment")


# Intermediate Project-Staff
class ProjectStaff(models.Model):
    project = models.ForeignKey('Project', on_delete=models.CASCADE)
    staff = models.ForeignKey('Staff', on_delete=models.CASCADE)


# Intermediate Event-Staff
class EventStaff(models.Model):
    event = models.ForeignKey('Event', on_delete=models.CASCADE)
    staff = models.ForeignKey('Staff', on_delete=models.CASCADE)


class Role(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Note(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField(default='')
    created = models.DateTimeField(default=datetime.now)
    order = models.IntegerField(default=0)
    id_staff = models.ForeignKey('Staff', related_name='id_staff', on_delete=models.CASCADE)
