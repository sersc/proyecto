from django.conf.urls import url

from . import views

urlpatterns = [
    # Index
    url(r'^$', views.get_projects, name='projects'),

    # Forms views
    url(r'^new_project', views.new_project, name='new_project'),
    url(r'^edit_project/(?P<project_id>[0-9]+)/$', views.edit_project, name="edit_project"),
    url(r'^new_issue/(?P<project_id>[0-9]+)/$', views.new_issue, name='new_issue'),
    url(r'^edit_issue/(?P<issue_id>[0-9]+)/$', views.edit_issue, name="edit_issue"),
    url(r'^new_event', views.new_event, name='new_event'),
    url(r'^edit_event/(?P<event_id>[0-9]+)/$', views.edit_event, name="edit_event"),
    url(r'^new_message', views.new_message, name='new_message'),

    # Create
    url(r'^create_project', views.create_project, name='create_project'),
    url(r'^create_issue/(?P<project_id>[0-9]+)/$', views.create_issue, name='create_issue'),
    url(r'^create_comment/(?P<issue_id>[0-9]+)/$', views.create_comment, name='create_comment'),
    url(r'^create_event/', views.create_event, name='create_event'),
    url(r'^create_message/', views.create_message, name='create_message'),

    # Update
    url(r'^update_project/(?P<project_id>[0-9]+)/$', views.update_project, name="update_project"),
    url(r'^update_issue/(?P<issue_id>[0-9]+)/$', views.update_issue, name="update_issue"),
    url(r'^update_event/(?P<event_id>[0-9]+)/$', views.update_event, name="update_event"),

    # Delete
    url(r'^delete_user/(?P<user_id>[0-9]+)/$', views.delete_user, name="delete_user"),
    url(r'^delete_project/(?P<project_id>[0-9]+)/$', views.delete_project, name="delete_project"),
    url(r'^delete_issue/(?P<issue_id>[0-9]+)/$', views.delete_issue, name="delete_issue"),
    url(r'^delete_event/(?P<event_id>[0-9]+)/$', views.delete_event, name="delete_event"),
    url(r'^delete_message/(?P<message_id>[0-9]+)/$', views.delete_message, name="delete_message"),

    # Models
    url(r'^users', views.get_users, name='users'),
    url(r'^projects', views.get_projects, name='projects'),
    url(r'^project/(?P<project_id>[0-9]+)/$', views.get_project, name='project'),
    url(r'^issue/(?P<issue_id>[0-9]+)/$', views.get_issue, name='issue'),
    url(r'^events', views.get_events, name='events'),
    url(r'^all_events', views.all_events, name='all_events'),
    url(r'^messages', views.get_messages, name='messages'),
    url(r'^message/(?P<message_id>[0-9]+)/$', views.get_message, name='message'),

    # Security
    url(r'^signin', views.sign_in, name='signin'),
    url(r'^register', views.register_view, name='register'),
    url(r'^create_user', views.create_user, name='create_user'),
    url(r'^update_profile/(?P<user_id>[0-9]+)/$', views.update_profile, name='update_profile'),
    url(r'^edit_profile/(?P<user_id>[0-9]+)/$', views.edit_profile, name='edit_profile'),
    url(r'^login', views.login_view, name='login'),
    url(r'^logout', views.logout_view, name='logout'),

    # Dashboard
    url(r'^dashboard', views.dashboard, name='dashboard'),
    url(r'^profile', views.profile, name='profile'),

    # Intermediate
    url(r'^add_staff_event/(?P<event_id>[0-9]+)/$', views.add_staff_event, name='add_staff_event'),
    url(r'^delete_staff_event/(?P<event_id>[0-9]+)/$', views.delete_staff_event, name='delete_staff_event'),
    url(r'^add_staff_project/(?P<project_id>[0-9]+)/$', views.add_staff_project, name='add_staff_project'),
    url(r'^delete_staff_project/(?P<project_id>[0-9]+)/$', views.delete_staff_project, name='delete_staff_project'),

    # Sidebar views
    url(r'^my_projects', views.my_projects, name='my_projects'),
    url(r'^my_events', views.my_events, name='my_events')

]
