from django.contrib import admin
from .models import Project, Issue, Event, Message, Staff, Role, Comment, EventStaff, ProjectStaff


admin.site.register(Project)
admin.site.register(Staff)
admin.site.register(Issue)
admin.site.register(Event)
admin.site.register(Message)
admin.site.register(Role)
admin.site.register(Comment)
admin.site.register(EventStaff)
admin.site.register(ProjectStaff)
