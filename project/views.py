from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.http import Http404, HttpResponse
from django.contrib import messages
from .models import Project, Event, Message, Staff, Comment, Issue, EventStaff, ProjectStaff, Role
from .forms import ProjectForm, EventForm, MessageForm, StaffForm, UserForm, IssueForm,\
    CommentForm, StaffForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from Proyecto.settings import EVENTS_PER_PAGE, PROJECTS_PER_PAGE, MESSAGES_PER_PAGE, ISSUES_PER_PAGE, USERS_PER_PAGE
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import datetime, date
from django.db.models import Q
from django.conf import settings

import json


# ============= GET ONE OBJECT ================= #


def get_users(request):
    if request.user.is_authenticated and str(request.user.staff.role) == "Admin":
        try:
            # Get users
            all_users = Staff.objects.all()
            page = request.GET.get('page', 1)
            paginator = Paginator(all_users, USERS_PER_PAGE)

            # Get page
            try:
                users = paginator.page(page)
            except PageNotAnInteger:
                users = paginator.page(1)
            except EmptyPage:
                users = paginator.page(paginator.num_pages)
        except Staff.DoesNotExist:
            raise Http404("That project does not exist.")

        context = {'users': users}
        if not users:
            context['message'] = "0 users found."

        return render(request, 'project/content/users.html', context)
    else:
        return redirect('signin')


def my_projects(request):
    if request.user.is_authenticated:
        try:
            # Get projects
            project_staff = ProjectStaff.objects.filter(staff=request.user.staff).values_list('project', flat=True)
            all_projects = Project.objects.filter(id__in=project_staff)
            page = request.GET.get('page', 1)
            paginator = Paginator(all_projects, PROJECTS_PER_PAGE)

            # Get page
            try:
                projects = paginator.page(page)
            except PageNotAnInteger:
                projects = paginator.page(1)
            except EmptyPage:
                projects = paginator.page(paginator.num_pages)
        except Project.DoesNotExist:
            raise Http404("That project does not exist.")

        context = {'projects': projects}
        if not projects:
            context['message'] = "0 projects found."

        return render(request, 'project/content/projects.html', context)
    else:
        return redirect('signin')


def get_projects(request):
    if request.user.is_authenticated:
        try:
            # Get projects
            search = request.GET.get('search', None)
            if search is not None:
                all_projects = Project.objects.filter(name__contains=search)
            else:
                all_projects = Project.objects.all()
            page = request.GET.get('page', 1)
            paginator = Paginator(all_projects, PROJECTS_PER_PAGE)

            # Get page
            try:
                projects = paginator.page(page)
            except PageNotAnInteger:
                projects = paginator.page(1)
            except EmptyPage:
                projects = paginator.page(paginator.num_pages)
        except Project.DoesNotExist:
            raise Http404("That project does not exist.")

        context = {'projects': projects}
        if not projects:
            context['message'] = "0 projects found."

        return render(request, 'project/content/projects.html', context)
    else:
        return redirect('signin')


def get_project(request, project_id):
    if request.user.is_authenticated:
        try:
            project = Project.objects.get(pk=project_id)

            # Get issues
            all_issues = Issue.objects.filter(project=project_id)
            page = request.GET.get('page', 1)
            paginator = Paginator(all_issues, ISSUES_PER_PAGE)

            project_staff = ProjectStaff.objects.filter(staff=request.user.staff).values_list('project', flat=True)

            # Get page
            try:
                issues = paginator.page(page)
            except PageNotAnInteger:
                issues = paginator.page(1)
            except EmptyPage:
                issues = paginator.page(paginator.num_pages)
        except Project.DoesNotExist:
            raise Http404("That project does not exist.")

        context = {'project': project, 'project_staff': project_staff}
        if not issues:
            context['message'] = "0 projects found."

        context['issues'] = issues
        return render(request, 'project/content/project.html', context)
    else:
        return redirect('signin')


def get_issue(request, issue_id):
    if request.user.is_authenticated:
        try:
            issue = Issue.objects.get(pk=issue_id)
        except Project.DoesNotExist:
            raise Http404("That issue does not exist.")

        context = {'issue': issue}
        return render(request, 'project/content/issue.html', context)
    else:
        return redirect('signin')


def all_events(request):
    if request.user.is_authenticated and str(request.user.staff.role) == 'Admin':
        try:
            # Get events
            all_events = Event.objects.all()
            page = request.GET.get('page', 1)
            paginator = Paginator(all_events, EVENTS_PER_PAGE)
            # Get page
            try:
                events = paginator.page(page)
            except PageNotAnInteger:
                events = paginator.page(1)
            except EmptyPage:
                events = paginator.page(paginator.num_pages)
        except Event.DoesNotExist:
            raise Http404("That event does not exist.")

        context = {'events': events}
        if not events:
            context['message'] = "0 events found."

        return render(request, 'project/content/all_events.html', context)
    else:
        return redirect('signin')


def my_events(request):
    if request.user.is_authenticated:
        try:
            # Get projects
            event_staff = EventStaff.objects.filter(staff=request.user.staff).values_list('event', flat=True)
            all_my_events = Event.objects.filter(id__in=event_staff)
            page = request.GET.get('page', 1)
            paginator = Paginator(all_my_events, EVENTS_PER_PAGE)

            # Get page
            try:
                events = paginator.page(page)
            except PageNotAnInteger:
                events = paginator.page(1)
            except EmptyPage:
                events = paginator.page(paginator.num_pages)
        except Project.DoesNotExist:
            raise Http404("That event does not exist.")

        context = {'events': events, 'event_staff': event_staff}
        if not events:
            context['message'] = "0 events found."

        return render(request, 'project/content/events.html', context)
    else:
        return redirect('signin')


def get_events(request):
    if request.user.is_authenticated:
        try:
            # Get events

            search = request.GET.get('search', None)
            if search is not None:
                event_list = Event.objects.filter(Q(date__gte=datetime.now())).filter(name__contains=search)
            else:
                event_list = Event.objects.filter(Q(date__gte=datetime.now()))

            event_staff = EventStaff.objects.filter(staff=request.user.staff).values_list('event', flat=True)
            page = request.GET.get('page', 1)
            paginator = Paginator(event_list, EVENTS_PER_PAGE)

            # Get page
            try:
                events = paginator.page(page)
            except PageNotAnInteger:
                events = paginator.page(1)
            except EmptyPage:
                events = paginator.page(paginator.num_pages)
        except Event.DoesNotExist:
            raise Http404("That event does not exist.")

        context = {'events': events, 'event_staff': event_staff}
        if not events:
            context['message'] = "0 upcoming events."

        return render(request, 'project/content/events.html', context)
    else:
        return redirect('signin')


def get_messages(request):
    if request.user.is_authenticated:
        try:
            # Get messages
            search = request.GET.get('search', None)
            if search is not None:
                all_messages = Message.objects.filter(receiver=request.user.staff).filter(subject__contains=search)
            else:
                all_messages = Message.objects.filter(receiver=request.user.staff)
            page = request.GET.get('page', 1)
            paginator = Paginator(all_messages, MESSAGES_PER_PAGE)

            # Get page
            try:
                messages = paginator.page(page)
            except PageNotAnInteger:
                messages = paginator.page(1)
            except EmptyPage:
                messages = paginator.page(paginator.num_pages)
        except Message.DoesNotExist:
            raise Http404("That message does not exist.")

        context = {'messages': messages}
        if not messages:
            context['message'] = "0 messages found."

        return render(request, 'project/content/messages.html', context)
    else:
        return redirect('signin')


def get_message(request, message_id):
    if request.user.is_authenticated:
        try:
            message = Message.objects.get(pk=message_id)
            if message.read is False:
                message.read = True
                message.save()
        except Message.DoesNotExist:
            raise Http404("That project does not exist.")

        context = {'message': message}
        return render(request, 'project/content/message.html', context)
    else:
        return redirect('signin')


# ============= DELETE VIEWS ================= #


def delete_user(request, user_id):
    if request.user.is_authenticated and str(request.user.staff.role) == "Admin":
        user = User.objects.get(pk=user_id)
        user.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return redirect('signin')


def delete_project(request, project_id):
    if request.user.is_authenticated:
        project = Project.objects.get(pk=project_id)
        project.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return redirect('signin')


def delete_issue(request, issue_id):
    if request.user.is_authenticated:
        issue = Issue.objects.get(pk=issue_id)
        issue.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return redirect('signin')


def delete_event(request, event_id):
    if request.user.is_authenticated and str(request.user.staff.role) == "Admin":
        event = Event.objects.get(pk=event_id)
        event.delete()
        return redirect('events')
    else:
        return redirect('signin')


def delete_message(request, message_id):
    if request.user.is_authenticated:
        message = Message.objects.get(pk=message_id)
        message.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return redirect('signin')


def delete_comment(request, comment_id):
    if request.user.is_authenticated:
        comment = Comment.objects.get(pk=comment_id)
        comment.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return redirect('signin')


# ============= DELETE VIEWS ================= #
# ============= FORM VIEWS ================= #


def new_project(request):
    if request.user.is_authenticated:
        form = ProjectForm()
        users = Staff.objects.all()
        context = {'form': form, 'users': users}
        return render(request, 'project/forms/new_project.html', context)
    else:
        return redirect('signin')


# Edit View
def edit_project(request, project_id):
    if request.user.is_authenticated:
        project = Project.objects.get(pk=project_id)
        users = Staff.objects.all()
        context = {'project': project, 'users': users}
        return render(request, 'project/forms/new_project.html', context)
    else:
        return redirect('signin')


# Create Action
def create_project(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = ProjectForm(request.POST, request.FILES)
            if form.is_valid():
                project = Project()
                project.name = form.cleaned_data['name']
                project.description = form.cleaned_data['description']
                project.status = form.cleaned_data['status']
                project.priority = form.cleaned_data['priority']
                project.language = form.cleaned_data['language']
                project.git_link = form.cleaned_data['git_link']
                project.created_by = request.user.staff
                project.save()
            else:
                return render(request, 'project/forms/new_project.html', {'form': form})

        return redirect('projects')
    else:
        return redirect('signin')


# Update Action
def update_project(request, project_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = ProjectForm(request.POST, request.FILES)
            project = Project.objects.get(pk=project_id)
            if form.is_valid():
                project = Project.objects.get(pk=project_id)
                project.name = form.cleaned_data['name']
                project.description = form.cleaned_data['description']
                project.status = form.cleaned_data['status']
                project.priority = form.cleaned_data['priority']
                project.language = form.cleaned_data['language']
                project.git_link = form.cleaned_data['git_link']
                project.save()
            else:
                messages.error(request, 'Project could not be updated')
                return render(request, 'project/forms/new_project.html', {'form': form, 'project': project})

        return get_project(request, project_id)
    else:
        return redirect('signin')


def add_staff_project(request, project_id):
    if request.user.is_authenticated:
        project = Project.objects.get(pk=project_id)
        project_staff = ProjectStaff(project=project, staff=request.user.staff)
        project_staff.save()

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return redirect('signin')


def delete_staff_project(request, project_id):
    if request.user.is_authenticated:
        project = Project.objects.get(pk=project_id)
        project_staff = ProjectStaff.objects.filter(project=project, staff=request.user.staff)
        project_staff.delete()

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return redirect('signin')


def new_issue(request, project_id):
    if request.user.is_authenticated:
        form = IssueForm()
        context = {'form': form, 'project_id': project_id}
        return render(request, 'project/forms/new_issue.html', context)
    else:
        return redirect('signin')


# Edit View
def edit_issue(request, issue_id):
    if request.user.is_authenticated:
        issue = Issue.objects.get(pk=issue_id)
        context = {'issue': issue}
        return render(request, 'project/forms/new_issue.html', context)
    else:
        return redirect('signin')


# Create Action
def create_issue(request, project_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = IssueForm(request.POST, request.FILES)
            if form.is_valid():
                project = Project.objects.get(pk=project_id)
                issue = Issue()
                issue.name = form.cleaned_data['name']
                issue.description = form.cleaned_data['description']
                issue.status = form.cleaned_data['status']
                issue.priority = form.cleaned_data['priority']
                issue.created_by = request.user.staff
                issue.last_update = datetime.now()
                issue.project = project
                issue.save()
            else:
                messages.error(request, 'Issue could not be created')
                return render(request, 'project/forms/new_issue.html', {'form': form})

        return get_project(request, project_id)
    else:
        return redirect('signin')


# Update Action
def update_issue(request, issue_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = IssueForm(request.POST, request.FILES)
            issue = Issue.objects.get(pk=issue_id)
            if form.is_valid():
                issue.name = form.cleaned_data['name']
                issue.description = form.cleaned_data['description']
                issue.status = form.cleaned_data['status']
                issue.priority = form.cleaned_data['priority']
                issue.last_update = datetime.now()
                issue.save()
            else:
                messages.error(request, 'Issue could not be updated')
                return render(request, 'project/forms/new_issue.html', {'form': form, 'issue': issue})

        return get_issue(request, issue_id)
    else:
        return redirect('signin')


# Create Action
def create_comment(request, issue_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = CommentForm(request.POST)
            issue = Issue.objects.get(pk=issue_id)
            if form.is_valid():
                comment = Comment()
                comment.text = form.cleaned_data['text']
                comment.date = datetime.now()
                comment.author = request.user.staff
                comment.issue = issue
                comment.save()
            else:
                return get_issue(request, issue_id)

        return get_issue(request, issue_id)
    else:
        return redirect('signin')


def new_event(request):
    if request.user.is_authenticated and str(request.user.staff.role) == "Admin":
        form = EventForm()
        context = {'form': form}
        return render(request, 'project/forms/new_event.html', context)
    else:
        return redirect('signin')


# Edit View
def edit_event(request, event_id):
    if request.user.is_authenticated and str(request.user.staff.role) == "Admin":
        event = Event.objects.get(pk=event_id)
        context = {'event': event}
        return render(request, 'project/forms/new_event.html', context)
    else:
        return redirect('signin')


# Create Action
def create_event(request):
    if request.user.is_authenticated and str(request.user.staff.role) == "Admin":
        if request.method == 'POST':
            form = EventForm(request.POST, request.FILES)
            if form.is_valid():
                event = Event()
                event.name = form.cleaned_data['name']
                event.description = form.cleaned_data['description']
                event.location = form.cleaned_data['location']
                event.date = form.cleaned_data['date']
                event.category = form.cleaned_data['category']
                event.assistance = form.cleaned_data['assistance']
                event.created_by = request.user.staff
                event.save()
            else:
                return render(request, 'project/forms/new_event.html', {'form': form})

        return redirect('events')
    else:
        return redirect('signin')


# Update Action
def update_event(request, event_id):
    if request.user.is_authenticated and str(request.user.staff.role) == "Admin":
        if request.method == 'POST':
            form = EventForm(request.POST, request.FILES)
            event = Event.objects.get(pk=event_id)
            if form.is_valid():
                event.name = form.cleaned_data['name']
                event.description = form.cleaned_data['description']
                event.location = form.cleaned_data['location']
                if form.cleaned_data["date"] is not None:
                    event.date = form.cleaned_data['date']
                event.category = form.cleaned_data['category']
                event.assistance = form.cleaned_data['assistance']
                event.save()
            else:
                messages.error(request, 'Event could not be updated')
                return render(request, 'project/forms/new_event.html', {'form': form, 'event': event})

        return redirect('events')
    else:
        return redirect('signin')


def add_staff_event(request, event_id):
    if request.user.is_authenticated:
        event = Event.objects.get(pk=event_id)
        staff = request.user.staff

        event_staff = EventStaff(event=event, staff=staff)
        event_staff.save()

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return redirect('signin')


def delete_staff_event(request, event_id):
    if request.user.is_authenticated:
        event = Event.objects.get(pk=event_id)
        event_staff = EventStaff.objects.filter(event=event, staff=request.user.staff)
        event_staff.delete()

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return redirect('signin')


def new_message(request):
    if request.user.is_authenticated:
        form = MessageForm()
        users = Staff.objects.all()
        context = {'form': form, 'users': users}
        return render(request, 'project/forms/new_message.html', context)
    else:
        return redirect('signin')


# Create Action
def create_message(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = MessageForm(request.POST, request.FILES)
            if form.is_valid():
                message = Message()
                message.subject = form.cleaned_data['subject']
                message.text = form.cleaned_data['text']
                message.date = datetime.now()
                message.read = False
                message.urgent = form.cleaned_data['urgent']
                message.sender = request.user.staff
                message.receiver = form.cleaned_data['receiver']
                message.save()
            else:
                return render(request, 'project/forms/new_message.html', {'form': form})

        return redirect('messages')
    else:
        return redirect('signin')

# ============= FORM VIEWS ================= #


def dashboard(request):
    if request.user.is_authenticated and str(request.user.staff.role) == "Admin":
        projects = Project.objects.count()
        events = Event.objects.count()
        active_events = Event.objects.filter(Q(date__gte=datetime.now()))
        issues = Issue.objects.count()
        users = Staff.objects.count()
        comments = Comment.objects.count()
        context = {'projects': projects, 'events': events, 'issues': issues,
                   'users': users, 'active_events': active_events, 'comments': comments}
        return render(request, 'project/dashboard/index.html', context)
    else:
        return redirect("signin")


def profile(request):
    if request.user.is_authenticated:
        staff = request.user.staff
        context = {'staff': staff}

        if str(request.user.staff.role) == "Admin":
            roles = Role.objects.all()
            context["roles"] = roles

        return render(request, 'project/dashboard/profile.html', context)
    else:
        return redirect("signin")


# Admin edit
def edit_profile(request, user_id):
    if request.user.is_authenticated and str(request.user.staff.role) == "Admin":
        staff = Staff.objects.get(pk=user_id)
        roles = Role.objects.all()

        return render(request, 'project/dashboard/profile.html', {
            'staff': staff,
            'roles': roles
        })
    else:
        return redirect("signin")


def update_profile(request, user_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            staff = Staff.objects.get(pk=user_id)
            user = staff.user
            roles = Role.objects.all()
            form = StaffForm(request.POST, request.FILES)

            if form.is_valid():
                if form.cleaned_data["birth_date"] is not None:
                    staff.birth_date = form.cleaned_data["birth_date"]
                user.first_name = request.POST["name"]
                user.last_name = request.POST["lastname"]
                staff.biography = form.cleaned_data["biography"]
                staff.role = form.cleaned_data["role"]
                staff.photo = form.cleaned_data["photo"]
                staff.save()
                user.save()
                messages.success(request, 'Profile updated sucessfully')

                return render(request, 'project/dashboard/profile.html', {
                    'staff': staff,
                    'roles': roles
                })
            else:
                messages.error(request, 'Please correct the error below.')

        return render(request, 'project/dashboard/profile.html', {
            'staff': staff
        })
    else:
        return redirect("signin")


'''
def update_profile(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            user_form = UserForm(request.POST)
            staff_form = StaffForm(request.POST, request.FILES)
            if staff_form.is_valid():
                user_form.save()
                staff_form.save()
                return redirect('profile')
            else:
                messages.error(request, 'Please correct the error below.')
        else:
            user_form = UserForm(instance=request.user)
            staff_form = StaffForm(instance=request.user.staff)

        return render(request, 'project/dashboard/profile.html', {
            'user_form': user_form,
            'staff_form': staff_form
        })

    else:
        return redirect('signin')
'''


# SECURITY views
def sign_in(request):
    if request.user.is_authenticated:
        return redirect('projects')
    else:
        return render(request, 'project/security/signin.html')


def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('projects')

    context = {'message': 'Invalid Username/Password'}
    return render(request, 'project/security/signin.html', context)


def logout_view(request):
    logout(request)
    return redirect('signin')


# ============= CREATE STAFF USER ================= #


def create_user(request):
    if request.method == 'POST':
        form = UserForm(request.POST, request.FILES)
        if form.is_valid():
            user = User.objects.create_user(
                username=form.cleaned_data['username'],
                email=form.cleaned_data['email'],
                password=form.cleaned_data['password']
            )

            user.save()
            login(request, user)
        else:
            return redirect('register')

    return redirect('signin')


def register_view(request):
    if request.user.is_authenticated:
        return redirect('projects')
    else:
        return render(request, 'project/security/register.html')


# Create Staff with 'Developer' Role as default
def create_staff(sender, instance, created, **kwargs,):
    user = instance
    role = Role.objects.get_or_create(name="Developer")
    if created:
        staff = Staff.objects.create(user=user)
        staff.role = role[0]
        staff.save()

        return redirect("signin")


post_save.connect(create_staff, sender=User, dispatch_uid="apps.models.create_staff")
