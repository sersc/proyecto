from django import forms
from .models import Project, Issue, Event, Message, Comment, Staff, EventStaff, ProjectStaff
from django.contrib.auth.models import User


class ProjectForm(forms.ModelForm):
    class Meta:
        fields = ('name', 'description', 'status', 'priority', 'language', 'git_link')
        model = Project


class IssueForm(forms.ModelForm):
    class Meta:
        fields = ('name', 'description', 'status', 'priority')
        model = Issue


class CommentForm(forms.ModelForm):
    class Meta:
        fields = ('text',)
        model = Comment


class EventForm(forms.ModelForm):
    class Meta:
        fields = ('name', 'description', 'location', 'category', 'assistance', 'date')
        model = Event


class MessageForm(forms.ModelForm):
    class Meta:
        fields = ('subject', 'text', 'date', 'read', 'urgent', 'receiver', 'sender')
        model = Message


class UserForm(forms.ModelForm):
    class Meta:
        fields = ('username', 'email', 'password')
        model = User


class StaffForm(forms.ModelForm):
    class Meta:
        model = Staff
        fields = ('photo', 'biography', 'role', 'birth_date')


class EventStaffForm(forms.ModelForm):
    class Meta:
        fields = ('event', 'staff')
        model = EventStaff


class ProjectStaffForm(forms.ModelForm):
    class Meta:
        fields = ('project', 'staff')
        model = ProjectStaff
