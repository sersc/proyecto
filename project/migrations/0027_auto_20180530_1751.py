# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-05-30 15:51
from __future__ import unicode_literals

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import project.models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0026_auto_20180530_1659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 5, 30, 17, 51, 38, 543532)),
        ),
        migrations.AlterField(
            model_name='staff',
            name='photo',
            field=models.ImageField(blank=True, default='', null=True, upload_to=project.models.profile_path),
        ),
        migrations.AlterField(
            model_name='staff',
            name='user',
            field=models.OneToOneField(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='user', to=settings.AUTH_USER_MODEL),
        ),
    ]
