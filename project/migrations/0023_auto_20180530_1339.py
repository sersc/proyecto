# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-05-30 11:39
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0022_auto_20180527_2002'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='date',
            field=models.DateField(default=datetime.date(2018, 5, 30), null=True),
        ),
        migrations.AlterField(
            model_name='message',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 5, 30, 13, 39, 48, 497749)),
        ),
    ]
